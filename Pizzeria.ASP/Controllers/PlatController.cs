﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Pizzeria.ASP.Models;
using Pizzeria.ASP.Security;
using Pizzeria.ASP.Services;
using Pizzeria.DAL;
using Pizzeria.DAL.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Pizzeria.ASP.Controllers
{
    [CustomAuthorization("Admin")]
    public class PlatController : Controller
    {
        private readonly PizzeriaContext _dc;
        private readonly IWebHostEnvironment _env;
        private readonly ICategorieService _catService;
        private readonly IPlatService _platService;

        public PlatController(
            ICategorieService catService,
            IPlatService platService,
            PizzeriaContext dc, 
            IWebHostEnvironment env)
        {
            _dc = dc;
            _env = env;
            _catService = catService;
            _platService = platService;
        }

        public IActionResult Index([FromQuery]int? filtre)
        {
            PlatCategorieModel model = new PlatCategorieModel
            {
                Filtre = filtre,
                Plats = _platService.GetAll(filtre),
                Categories = _catService.GetAll()
            };

            return View(model);
        }

        public IActionResult Create()
        {
            PlatAddModel model = new PlatAddModel {
                Categories = _catService.GetAll()
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(PlatAddModel form)
        {
            if(ModelState.IsValid)
            {
                _platService.Create(form);
                TempData["success"] = "OK";
                return RedirectToAction("Index");
            }

            form.Categories = _catService.GetAll();

            return View(form);

        }

        public IActionResult Delete(int id)
        {
            Plat plat = _platService.Delete(id);
            return RedirectToAction("Index");
        }
    }
}

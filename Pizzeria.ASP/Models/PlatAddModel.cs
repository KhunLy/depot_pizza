﻿using Microsoft.AspNetCore.Http;
using Pizzeria.ASP.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Pizzeria.ASP.Models
{
    public class PlatAddModel
    {
        [Required]
        [MaxLength(50)]
        public string Nom { get; set; }

        [Required]
        [RegularExpression(@"[0-9]{1,6}([.,][0-9]{0,2})?")]
        public string Prix { get; set; }

        public string Description { get; set; }

        [Required]
        public int CategorieId { get; set; }

        [FileMimeType("image/jpg", "image/jpeg", "image/png", "image/x-icon")]
        public IFormFile File { get; set; }

        public IEnumerable<CategorieModel> Categories { get; set; }
    }
}
